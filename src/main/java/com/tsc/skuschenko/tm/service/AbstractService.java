package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.model.AbstractEntity;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> entityRepository;

    @Override
    public void add(@Nullable final E entity) {
        Optional.ofNullable(entity)
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entityRepository.add(entity);
    }

    @Override
    public void addAll(@NotNull List<E> entity) {
        entityRepository.addAll(entity);
    }

    @Override
    public void clear() {
        entityRepository.clear();
    }

    @Nullable
    @Override
    public List<E> findAll() {
        return entityRepository.findAll();
    }

    @Nullable
    @Override
    public E findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.findById(id);
    }

    @Override
    public void remove(@Nullable final E entity) {
        Optional.ofNullable(entity)
                .orElseThrow(() -> new EntityNotFoundException(
                        this.getClass().getSimpleName()));
        entityRepository.remove(entity);
    }

    @Nullable
    @Override
    public E removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.removeById(id);
    }

}
