package com.tsc.skuschenko.tm.component;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.bootstrap.Bootstrap;
import com.tsc.skuschenko.tm.command.data.BackupLoadCommand;
import com.tsc.skuschenko.tm.command.data.BackupSaveCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    @SneakyThrows
    private void load() {
        bootstrap.parseCommand(BackupLoadCommand.NAME);
    }

    @SneakyThrows
    @Override
    public void run() {
        while (true) {
            save();
            @Nullable final IPropertyService propertyService =
                    bootstrap.getPropertyService();
            final int timeout = propertyService.getBackupTime();
            Thread.sleep(timeout);
        }
    }

    @SneakyThrows
    private void save() {
        bootstrap.parseCommand(BackupSaveCommand.NAME);
    }

}
