package com.tsc.skuschenko.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.tsc.skuschenko.tm.dto.Domain;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.EmptyBinaryPathException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "load data from yaml file";

    @NotNull
    private static final String NAME = "data-load-yaml";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileYamlPath();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyBinaryPathException::new);
        @NotNull final String json = new String(
                Files.readAllBytes(Paths.get(filePath))
        );
        @NotNull final ObjectMapper objectMapper =
                new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain =
                objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
