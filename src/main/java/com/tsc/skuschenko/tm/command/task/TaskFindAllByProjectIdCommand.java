package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public final class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "find all task by project id";

    @NotNull
    private static final String NAME = "find-all-task-by-project-id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("project id");
        @NotNull final String value = TerminalUtil.nextLine();
        @NotNull final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        @Nullable final List<Task> tasks
                = projectTaskService.findAllTaskByProjectId(userId, value);
        Optional.ofNullable(tasks).filter(item -> item.size() != 0)
                .ifPresent(item -> item
                        .forEach(this::showTask));
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
